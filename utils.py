"""
des: covert 0~9 to 零~九 + 计算拼音宽度
author: mr52hz
date: 2021-11-06
"""
import settings
import re


num = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九']
kin = ['十', '百', '千', '万', '零']

num_dic = {
    '0': '零',
    '1': '一',
    '2': '二',
    '3': '三',
    '4': '四',
    '5': '五',
    '6': '六',
    '7': '七',
    '8': '八',
    '9': '九',
    '10': '十',
    '11': '十一',
    '12': '十二',
    '13': '十三',
    '14': '十四',
    '15': '十五',
    '16': '十六',
    '17': '十七',
    '18': '十八',
    '19': '十九',
}


def sadd(x):
    x.reverse()
    if len(x) >= 2:
        x.insert(1, kin[0])
        if len(x) >= 4:
            x.insert(3, kin[1])
            if len(x) >= 6:
                x.insert(5, kin[2])
                if len(x) >= 8:
                    x.insert(7, kin[3])
                    if len(x) >= 10:
                        x.insert(9, kin[0])
                        if len(x) >= 12:
                            x.insert(11, kin[1])

    x = fw(x)
    x = d1(x)
    x = d2(x)
    x = dl(x)
    x.reverse()
    x = ''.join(x)
    return x


def d1(x):
    if '零' in x:
        a = x.index('零')
        if a == 0:
            del x[0]
            d1(x)
        else:
            if x[a + 2] in ['十', '百', '千', '万', '零']:
                if x[a + 1] != '万':
                    del x[a + 1]
                    d1(x)
    return x


def d2(x):
    try:
        a = x.index('零')
        if x[a - 1] in ['十', '百', '千', '零']:
            del x[a - 1]
            d2(x[a + 1])
    except:
        pass
    return x


def fw(x):
    if len(x) >= 9:
        if x[8] == '零':
            del x[8]
    return x


def dl(x):
    try:
        if x[0] == '零':
            del x[0]
            dl(x)

        # ['一', '十', '六'] ==> ['十', '六']
        if len(x) == 3 and x[-1] == '一':
            print(x)
            del x[-1]

    except:
        pass
    return x


def num_to_zh_num(integer):
    integer = str(int(integer))
    result = []
    _integer = int(integer)

    if 1930 <= _integer <= 2100:
        # 年份
        for c in list(integer):
            result.append(num_dic[c])
        return ''.join(result)

    if 0 <= _integer <= 19:
        return num_dic[integer]

    for c in list(integer):
        result.append(num[int(c)])
    return sadd(result)


def line_num_replace(line):
    nums = re.findall(r'\d+', line)
    if len(nums) == 0:
        return line
    n = []
    for num in nums:
        d = num_to_zh_num(num)
        n.append(d)
        line = line.replace(num, d, 1)

    return line


def cal_pinyin_width(pinyin):
    # 根据拼音长度计算label宽度
    if pinyin == '/':
        width = 10
    elif len(pinyin) <= 2:
        width = settings.PY_LABEL_MIN_WIDTH
    elif len(pinyin) > 2 and pinyin[-1] not in ['1', '2', '3', '4', '5']:
        # 计算英文大小
        width = (len(pinyin) - 2) * settings.PER_PY_LABEL_INTERVAL + settings.PY_LABEL_MIN_WIDTH
    else:
        w = (len(pinyin) - 2) * settings.PER_PY_LABEL_INTERVAL + settings.PY_LABEL_MIN_WIDTH
        if w > settings.PY_LABEL_MAX_WIDTH:
            width = settings.PY_LABEL_MAX_WIDTH
        else:
            width = w
    return width


if __name__ == '__main__':
    # 4位 3位 2位 1位
    text = '2010,117,12,20,6,8000'
    print(line_num_replace(text))
    assert line_num_replace(text) == '二零一零,一百一十七,十二,二十,六,八千'
