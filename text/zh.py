"""
des: 中文对象
author: mr52hz
date: 2021-10-30
"""
import re

from text.split import zhAddBackslash


class Zh(object):

    def __init__(self, zh, is_add_backslash=False):
        """
        中文对象
        :param zh: 中文字符串
        """
        if '/' in zh or is_add_backslash is False:
            # 包含backslash
            self._zh_backslash = zh
            self._zh = ''.join(zh.split('/'))
        else:
            self._zh = zh
            self._zh_backslash = zhAddBackslash(zh)

    @property
    def isContainAlpha(self):
        return bool(re.search(r'[a-zA-Z0-9]', self._zh))

    @property
    def zhStr(self):
        return self._zh

    @property
    def backslashStr(self):
        return self._zh_backslash

    @property
    def zhLength(self):
        return len(self._zh_bits())

    @property
    def zhBackslashLength(self):
        return len(self._zh_bits(contain_backslash=True))

    @property
    def zhIter(self):
        for char in self._zh_bits():
            yield char

    @property
    def zhBackslashIter(self):
        for bit in self._zh_bits(contain_backslash=True):
            yield bit

    def _zh_bits(self, contain_backslash=False):
        """
        中文长度 time字母单词长度为 1
        :return:
        """
        bits = []
        buffer = ''
        for _char in self.backslashStr:
            if _char is ' ':
                # 只一个允许 出现在 字母后
                if buffer != '':
                    buffer += _char
                    bits.append(buffer)
                    buffer = ''
            elif bool(re.search(r'[a-zA-Z0-9]', _char)):
                # 字母 数字 写入buffer
                buffer += _char
            elif _char == '/':
                if contain_backslash is True:
                    if buffer:
                        bits.append(buffer)
                        buffer = ''
                    bits.append(_char)
                else:
                    if buffer:
                        bits.append(buffer)
                        buffer = ''
            else:
                # 当前不是数字 字母
                if buffer == '':
                    # 缓存中没有字符
                    bits.append(_char)
                else:
                    # 有字符时 需要+1 而且当前不为字母 数字 +1
                    bits.append(buffer)
                    buffer = ''
                    bits.append(_char)
        if buffer != '':
            bits.append(buffer)
            buffer = ''
        return bits


if __name__ == '__main__':
    t = '为人就爱上了，民APP服务time'
    zh_obj = Zh(t, is_add_backslash=True)
    print(zh_obj.zhStr)
    print(zh_obj.zhLength)
    print(zh_obj.backslashStr)
    # for char in zh_obj.zhIter:
    #     print(char)

