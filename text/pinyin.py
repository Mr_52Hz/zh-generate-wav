"""
des: zh->拼音
author: mr52hz
date: 2021-10-29
"""
import re
from pypinyin import pinyin, Style

import settings
from text.zh import Zh


class Pinyin(object):

    def __init__(self, zh_obj, py_ls=None):
        """
        拼音生成器
        :param zh_obj: <Zh>中文对象
        """
        self.zh = zh_obj
        self.pinyin_ls = []

        if py_ls is None:
            self._transform()
        else:
            self.pinyin_ls = py_ls

    def insert(self, index, value):
        if index < self.pinyinLength:
            self.pinyin_ls[index].insert(0, value)

    @property
    def pinyinStr(self):
        return ' '.join([py[0] for py in self.pinyin_ls])

    @property
    def pinyinLength(self):
        return len(self.pinyin_ls)

    @property
    def zhAndPinYinCharIter(self):
        for zh_char, py, idx in zip(self.zh.zhBackslashIter, self.pinyin_ls, range(0, self.pinyinLength)):
            yield zh_char, py, idx

    def _transform(self):
        if self.zh.isContainAlpha:
            # 筛选 字母 数字 和纯中文（不含/）
            alpha_ls = []
            zh_str = ''
            for idx, char in enumerate(self.zh.zhIter):
                if bool(re.search(r'[a-zA-Z0-9]', char)):
                    alpha_ls.append((idx, char))
                else:
                    zh_str += char
            self.pinyin_ls = pinyin(zh_str, style=Style.TONE3, heteronym=settings.is_open_multi_pinyin,
                                    neutral_tone_with_five=True)
            # 1、调整标点
            self._adjustSymbol()
            # 将字母 数字添加 回去
            for idx, alpha in alpha_ls:
                self.pinyin_ls.insert(idx, [alpha])
            # 3、给重点注意的汉字添加focus标识
            self._addMultiZhFocusSign(self.zh.zhIter, self.pinyin_ls)
            # 4、将'/'符号添加到原来的位置
            self._addBackslash()

        else:
            # 纯汉字-不包含'/'分割-提高pypinyin的翻译准确度
            self.pinyin_ls = pinyin(self.zh.zhStr, style=Style.TONE3, heteronym=settings.is_open_multi_pinyin,
                                    neutral_tone_with_five=True)
            # 1、调整标点
            self._adjustSymbol()
            # 2、先调整之后再给重点注意的汉字添加focus标识
            self._addMultiZhFocusSign(self.zh.zhIter, self.pinyin_ls)
            # 将'/'符号添加到原来的位置
            self._addBackslash()

    def _adjustSymbol(self):
        # 调整拼音列表中的符号 标点列表 ['》，'] --> ['》'] ['，']
        for index, py in enumerate(self.pinyin_ls):
            rtn_value = self._split(py)
            if rtn_value:
                self._insert(self.pinyin_ls, index, rtn_value)

    def _addBackslash(self):
        # 添加默认的'/'分割符号
        for index, item in enumerate(self.zh.zhBackslashIter):
            if item == '/':
                self.pinyin_ls.insert(index, ['/'])

    def adjustBackslash(self, zh):
        # 根据新的zh 调整 并更新中文对象
        self.zh = zh
        # 删除 /
        self.pinyin_ls = [py for py in self.pinyin_ls if py != ['/']]
        if len(self.pinyin_ls) == self.zh.zhLength:
            # print('adjust')
            # 拼音分词相同 则 再添加回去 根据新的中文对象
            self._addBackslash()
        else:
            # print('remake')
            # 分词改变了 拼音列表
            self._transform()

    @staticmethod
    def _addMultiZhFocusSign(zhIter, py):
        # 为多音字添加focus
        for idx, char in enumerate(zhIter):
            if char in settings.multi_py_hans:
                py[idx].append('focus')

    @staticmethod
    def _split(ls):
        # ['》，'] --> ['》'] ['，']
        if not len(ls) == 1:
            # 排除多音--多标点一定是长度为1的列表
            return None
        if len(ls[0]) > 1 and ls[0][-1] not in ['1', '2', '3', '4', '5']:
            # 多标点 长度大于1 并且不能够是拼音-拼音特定是结尾是1-5的数字
            # 列表推导式 分裂列表
            new_ls = [[i] for i in ls[0] if i is not ' ']
            if len(new_ls) > 1:
                return new_ls
            else:
                return None
        else:
            return None

    @staticmethod
    def _insert(ls, index, val):
        # 在列表特定位置插入多个列表
        # [...['，。']...] --> [...['，'], ['。']...]
        # 先删除index元素
        ls.pop(index)
        for i in range(len(val)):
            ls.insert(index + i, val[i])


if __name__ == '__main__':

    # t = '为人就爱上了，民APP服务time'
    t = '而/这里/A/P/P/追叙“昔/游”时/却用/了/一个“悲”字。'
    zh = Zh(t)
    zh_py = Pinyin(zh)
    # adjust
    # zh2 = Zh('而/这里/A/P/P/追叙“昔游”时/却用/了/一个“悲”字。')
    # remake
    zh2 = Zh('而/这里/AP/P/追叙“昔/游”时/却用/了/一个“悲”字。')
    zh_py.adjustBackslash(zh2)
    print(zh.zhStr)
    print(zh.backslashStr)
    print(zh_py.zh.backslashStr)
    print(zh_py.pinyin_ls)
    print(len(zh_py.pinyin_ls) == zh_py.zh.zhBackslashLength)



