"""
des: 解析txt csv
author: mr52hz
date: 2021-10-29
"""
import os
import re
from abc import ABC, abstractmethod

import settings
from text.split import paragraphToSentences
from text.pinyin import Pinyin
from text.zh import Zh
from utils import line_num_replace


class Parser(ABC):

    def __init__(self, path):
        self.path = path
        self.root, self.file_name = os.path.split(self.path)
        name, extension = os.path.splitext(self.file_name)
        self.cache_home = os.path.join(self.root, name + '-cache')
        if not os.path.exists(self.cache_home):
            os.makedirs(self.cache_home)
        self.wavs_temp = os.path.join(self.cache_home, 'wavs-'+settings.LOG_VERSION)
        if not os.path.exists(self.wavs_temp):
            os.makedirs(self.wavs_temp)
        self.zh_lines = []
        self.pinyin_lines = []

    @property
    def height(self):
        return len(self.zh_lines)

    @property
    def isPinyinReady(self):
        return len(self.zh_lines) > 0 and len(self.zh_lines) == len(self.pinyin_lines)

    def append(self, zh, py):
        self.zh_lines.append(zh)
        self.pinyin_lines.append(py)

    def pop(self, index):
        if index < self.height:
            self.zh_lines.pop(index)
            self.pinyin_lines.pop(index)

    def insertEmptyZhPyObj(self, index):
        zh = Zh(zh='！空白行！', is_add_backslash=False)
        pinyin = Pinyin(zh, py_ls=[['！'], ['kong4'], ['bai2'], ['xing2'], ['！']])
        self.zh_lines.insert(index, zh)
        self.pinyin_lines.insert(index, pinyin)

    @abstractmethod
    def parse(self):
        pass

    @property
    def isEmpty(self):
        with open(self.path, 'r', encoding='utf-8') as f:
            paragraph = f.read()
        if bool(re.search(r'[a-zA-Z\u4e00-\u9fa5]', paragraph)):
            return False
        else:
            return True


class TxtParser(Parser):

    def __init__(self, path):
        super(TxtParser, self).__init__(path)

    def parse(self):
        with open(self.path, 'r', encoding='utf-8') as f:
            paragraph = f.read()
            # 处理去掉英文标点和换行
            for key, value in settings.chinese_full_dict.items():
                paragraph = re.sub(key, value, paragraph)

            for s in paragraphToSentences(paragraph):
                if bool(re.search(r'[0-9]', s)):
                    # 0-9 替换掉
                    s = line_num_replace(s)
                zh = Zh(s, is_add_backslash=True)
                pinyin = Pinyin(zh)
                self.zh_lines.append(zh)
                self.pinyin_lines.append(pinyin)


class CsvParser(Parser):

    def __init__(self, path):
        super(CsvParser, self).__init__(path)

    def parse(self):
        pass
