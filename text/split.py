# coding:utf-8
"""
des: 中文段落分句 + 中文分词
author: mr52hz
date: 2021-10-29
"""
import re
import settings
from jieba import posseg


def paragraphToSentences(para):
    # 将句子按照终止符切割
    # 1、先分割非引号 结尾的终止符
    para = re.sub('([。！？；])([^”’])', r"\1\n\2", para)  # 单字符断句符
    # 2、省略切割
    para = re.sub('(\…{2})([^”’])', r"\1\n\2", para)  # 中文省略号
    # 如果双引号前有终止符，那么双引号才是句子的终点，把分句符\n放到双引号后，注意前面的几句都小心保留了双引号
    para = re.sub('([。！？][”’])([^，。！？])', r'\1\n\2', para)
    para = para.rstrip()  # 段尾如果有多余的\n就去掉它
    sentences = [sentence for sentence in para.split("\n") if sentence]
    return adjustSentences(sentences)


def longSentenceSplit(sentence, length):
    # 长句子分割 句子长度
    sentence = re.sub('([，：])([^”’])', r"\1\n\2", sentence)
    all_sentences = [_s for _s in sentence.strip().split('\n') if _s]
    # 惰性切割 将长度相加 符合最大限度的合并
    sentences = []
    buffer = ''
    for cut_sentence in all_sentences:
        if len(buffer+cut_sentence) <= length / 2:
            buffer += cut_sentence
        else:
            if buffer:
                sentences.append(buffer)
            buffer = cut_sentence
    if buffer:
        sentences.append(buffer)

    if len(sentences) == 1:
        sentence = sentences[0]
        sentences = [sentence[:settings.SENTENCE_MAX_LEN], sentence[settings.SENTENCE_MAX_LEN:]]
    return adjustSentences(sentences)


def adjustSentences(sentences):
    # 调整句子长度
    # 将短句合并 如果前后为长句 则忽略
    # 将长句分割

    fit_length_sentences = []
    sentence_buffer = ''
    for sentence in sentences:
        sentence = sentence.strip()

        if sentence_buffer:
            sentence = sentence_buffer + sentence

        # 计算长度 只计算汉字
        _sentence_zh = re.sub('[a-zA-Z]', '', sentence)
        sentence_len = len(_sentence_zh)

        if settings.SENTENCE_MIN_LEN <= sentence_len <= settings.SENTENCE_MAX_LEN:
            fit_length_sentences.append(sentence)
            sentence_buffer = ''
        elif settings.SENTENCE_MIN_LEN > sentence_len:
            # 向后合并
            sentence_buffer = sentence
        else:
            # 切割
            fit_sentences = longSentenceSplit(sentence, sentence_len)
            # print(sentence, ' to ', '+'.join(fit_sentences))
            if len(fit_sentences[-1]) < settings.SENTENCE_MIN_LEN:
                fit_length_sentences.extend(fit_sentences[:-1])
                sentence_buffer = fit_sentences[-1]
            else:
                fit_length_sentences.extend(fit_sentences)
                sentence_buffer = ''

    if sentence_buffer:
        fit_length_sentences.append(sentence_buffer)

    return fit_length_sentences


def zhAddBackslash(zh):
    if not zh:
        return zh
    seg_words = posseg.cut(zh)
    seg_zh = ''
    for word in seg_words:
        word, flag = word.word, word.flag
        if flag == 'x':
            # 去掉标点之前的分隔符
            if seg_zh and seg_zh[-1] == '/':
                seg_zh = seg_zh[:-1] + word
            else:
                seg_zh += word
        else:
            seg_zh += (word+'/')
    # 去掉最后一个分隔符
    if seg_zh and seg_zh[-1] == '/':
        seg_zh = seg_zh[:-1] + '，'
    return seg_zh


if __name__ == '__main__':
    p = """诗的。首句“迟日园林悲昔游”，是因眼前的春光回忆起往昔的春游，当年，春日迟迟、园林如绣、游目骋怀、该是心旷神怡的。
    而这里追叙“昔游”时却用了一个“悲”字。这个悲，是今天的悲，是从今天的悲追溯昔日的乐；而反过来，也可以说，正因为想起当时的游乐，
    就更觉得当前处境之可悲。吴乔在《围炉诗话》中说：“情能移境，境亦能移情。”这一句诗是用现在的情移过去的境，
    为昔日的欢乐景物注入了今天的悲伤心情。"""
    total_s = 0
    for s in paragraphToSentences(p):
        total_s += len(s)
        print(zhAddBackslash(s))
        # break

    print('total len: ', len(p))
    print('split len: ', total_s)

