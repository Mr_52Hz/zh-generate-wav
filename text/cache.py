"""
des: json cache file
author: mr52hz
date: 2021-11-09
"""
import json
import os
from abc import ABC

import settings
from text import Zh, Pinyin


class JsonKey(ABC):

    def __init__(self):
        # json key
        self.mode_key = 'mode'
        self.current_line_index_key = 'currentLine'
        self.zh_key = 'zh'
        self.py_key = 'py'
        self.who_key = 'who'
        self.duration_key = 'quiet_duration'
        self.emotion_key = 'emotion'
        self.color_key = 'color'
        self.is_edited_key = 'isEdited'
        self.is_locked_key = 'isLocked'
        self.is_checked_key = 'isChecked'
        self.lines_key = 'lines'


class Cache(JsonKey):

    __instance = None

    def __new__(cls, *args, **kwargs):
        if Cache.__instance is None:
            Cache.__instance = super().__new__(cls)
        return Cache.__instance

    def __init__(self, file):
        super(Cache, self).__init__()
        self.path = file
        self.root, self.file_name = os.path.split(self.path)
        name, extension = os.path.splitext(self.file_name)
        self.cache_home = os.path.join(self.root, name + '-cache')
        self.json_file = os.path.join(self.cache_home, name+'-'+settings.LOG_VERSION+'.log')
        self.json = {'mode': '', 'currentLine': '0', 'lines': {}}
        self._is_loaded = False
        # 锁定后 无法写入 只能读取
        self._is_locked = False

    @property
    def hasCached(self):
        is_exists = os.path.exists(self.cache_home) and os.path.exists(self.json_file)

        if is_exists is False:
            self.dumps()
            return False
        else:
            if self.check():
                return True

        return False

    @property
    def isLocked(self):
        return self._is_locked

    @isLocked.setter
    def isLocked(self, value):
        self._is_locked = value

    def load(self):
        if self._is_loaded is False:
            with open(self.json_file, 'r', encoding='UTF-8') as f:
                self.json = json.load(f)
                self._is_loaded = True

    def dumps(self):
        if self.isLocked is True:
            return
        # 写入缓存
        with open(self.json_file, 'w', encoding='UTF-8') as f:
            s = json.dumps(self.json)
            f.write(s)

    def check(self):
        self.load()
        mode = self.json.get(self.mode_key, None)
        if mode is None or mode not in ['分词校验', 'wav校验']:
            return False

        current_line = self.json.get(self.current_line_index_key, None)
        if current_line is None:
            return False

        lines = self.json.get(self.lines_key, {})
        if len(lines.keys()) == 0 or '0' not in lines.keys():
            return False
        line_dic = lines['0']
        if 'color' not in line_dic.keys():
            return False

        return True

    @property
    def height(self):
        return len(self.json[self.lines_key].keys())

    @property
    def mode(self):
        self.load()
        return self.json[self.mode_key]

    @mode.setter
    def mode(self, value):
        if self.isLocked is True:
            return

        self.json[self.mode_key] = value
        self.dumps()

    @property
    def currentLineIndex(self):
        self.load()
        return int(self.json[self.current_line_index_key])

    @currentLineIndex.setter
    def currentLineIndex(self, value):
        if self.isLocked is True:
            return

        self.json[self.current_line_index_key] = value
        self.dumps()

    def addLine(self, line, dump=False):
        """
        注意调用时 每行 不进行dumps要统一 调用
        :param line:
        :param dump:
        :return:
        """
        if self.isLocked is True:
            return

        self.json[self.lines_key][str(line.index)] = {
            self.zh_key: line.zh.backslashStr,
            self.py_key: line.pinyin.pinyin_ls,
            self.who_key: line.who_combo.currentText(),
            self.emotion_key: line.emotion_combo.currentText(),
            self.color_key: line.color_combo.currentText(),
            self.duration_key: line.duration_combo.currentText(),
            self.is_locked_key: line.isLocked,
            self.is_edited_key: line.isEdited,
            self.is_checked_key: line.isTextChecked,
        }
        if dump is True:
            self.dumps()

    def insertLine(self, line):
        for index in range(self.height-1, line.index-1, -1):
            self.json[self.lines_key][str(index + 1)] = self.getIndexLineDict(index)
        self.addLine(line, dump=True)

    def deleteLine(self, line):
        for index in range(line.index+1, self.height):
            self.json[self.lines_key][str(index-1)] = self.getIndexLineDict(index)
        self.json[self.lines_key].pop(str(self.height-1))
        self.dumps()

    def changeLineState(self, idx, key, value):
        if self.isLocked is True:
            return

        idx = str(idx)

        if idx not in self.json[self.lines_key].keys():
            return

        if key == 'edit':
            self.json[self.lines_key][idx][self.is_edited_key] = value
        elif key == 'check':
            self.json[self.lines_key][idx][self.is_checked_key] = value
        elif key == 'lock':
            self.json[self.lines_key][idx][self.is_locked_key] = value
        elif key == 'who':
            self.json[self.lines_key][idx][self.who_key] = value
        elif key == 'emotion':
            self.json[self.lines_key][idx][self.emotion_key] = value
        elif key == 'color':
            self.json[self.lines_key][idx][self.color_key] = value
        elif key == 'duration':
            self.json[self.lines_key][idx][self.duration_key] = value

        self.dumps()

    def changeLineContent(self, idx, zh, py):
        """
        改变某一行 内容
        :param idx: line index
        :param zh: Zh对象 or None
        :param py: Pinyin对象
        :return: None
        """
        if self.isLocked is True:
            return

        idx = str(idx)

        if idx not in self.json[self.lines_key].keys():
            return

        if zh is not None:
            self.json[self.lines_key][idx][self.zh_key] = zh.backslashStr
        self.json[self.lines_key][idx][self.py_key] = py.pinyin_ls

        self.dumps()

    def getIndexLineDict(self, index):
        if index < self.height:
            return self.json[self.lines_key][str(index)]

    def getZhAndPyOjbIter(self):
        for index in range(self.height):
            line_dict = self.getIndexLineDict(index)
            zh_obj = Zh(zh=line_dict[self.zh_key], is_add_backslash=False)
            py_str_ls = line_dict[self.py_key]
            py_obj = Pinyin(zh_obj=zh_obj, py_ls=py_str_ls)
            yield index, zh_obj, py_obj, line_dict


if __name__ == '__main__':
    j = r"C:\Users\whaty-ai\Desktop\test_zh_wav\7.txt"
    cache = Cache(j)
    cache.load()

    print(cache.json['mode'])
    print(cache.check())
    cache.json['mode2'] = False
    cache.dumps()
    print(cache.json.get('mode2', 'k'))