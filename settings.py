"""
des: zh-generate-wav配置文件
author: mr52hz
date: 2021-10-29
"""
import os
from PyQt5.QtCore import QSettings

LOG_VERSION = 'V0.0.2'

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

USER_HOME = os.path.expanduser('~')

DESKTOP_DIR = os.path.join(USER_HOME, 'Desktop')

DOCUMENT_DIR = os.path.join(USER_HOME, 'Documents')

APP_DIR = os.path.join(DOCUMENT_DIR, 'zh-generate-wav')

if not os.path.exists(APP_DIR):
    os.makedirs(APP_DIR)


q_st = QSettings(os.path.join(APP_DIR, 'config-%s.ini' % LOG_VERSION), QSettings.IniFormat)
q_st.setIniCodec('utf-8')


def get_or_create_setting(key, default):
    value = q_st.value(key, '--')
    if value == '--':
        q_st.setValue(key, default)
        value = default
    return value


# theme
theme_font_size = int(get_or_create_setting('theme/font-size', 11))
theme_window_size = get_or_create_setting('theme/window', None)
# 句子长度限制
SENTENCE_MAX_LEN = int(get_or_create_setting('sentence/max-len', 50))
SENTENCE_MIN_LEN = int(get_or_create_setting('sentence/min-len', 10))

# 拼音label宽度
PY_LABEL_MIN_WIDTH = 25
PY_LABEL_MAX_WIDTH = 54
PER_PY_LABEL_INTERVAL = 9


# 拼音
is_open_multi_pinyin = True if get_or_create_setting('pinyin/open-multi', 'on') == 'on' else False
multi_py_hans = ['长', '调', '地', '勒', '为', '鲜', '兴', '便', '得', '的', '行', '重', '种', '藏', '朝', '处',
                 '给', '血', '卡', '识', '着', '转']
multi_py_hans_str = get_or_create_setting('pinyin/multi-pinyin-focus', ' '.join(multi_py_hans))
multi_py_hans = [han for han in multi_py_hans_str if '\u4e00' <= han <= '\u9fff']

# who emotions
who_dic = {'亮亮': 'FangLiang', '芳芳': 'YuFang',
           '贝贝': 'BeiBei', '橙橙': 'MuZiCheng',
           '英英': 'LJSpeech',
           }
who = ['亮亮', '橙橙', '芳芳', '贝贝']
who_str = get_or_create_setting('emotion/who', ' '.join(who))
who = [w for w in who_str.split(' ')]

# emotion_a = ['自然', '喜悦', '焦急', '恐惧', '疑惑', '生气', '激动', '邪恶', '调侃', '悲伤', '吟诵']
# emotion_ = get_or_create_setting('emotion/emotion_a', ' '.join(emotion_a))

emotion_str = get_or_create_setting('emotion/emotions', '自然')
emotions = [e for e in emotion_str.split(' ') if e]

# color_a = ['旁白', '女声', '童声', '青年', '中年', '老人']
# color_ = get_or_create_setting('emotion/color_a', ' '.join(color_a))

color_str = get_or_create_setting('emotion/colors', '旁白')
colors = [c for c in color_str.split(' ') if c]

# wav空白长度
durations = ['0.2', '0.5', '1.0', '3.0']
duration_str = get_or_create_setting('wav/quiet-interval', ' '.join(durations))
wav_quiet_durations = [d for d in duration_str.split(' ') if d]

# generate backend msg
generate_url = 'http://192.168.20.8:8083'
generate_url = get_or_create_setting('backend/url', generate_url)

# last path
recent_path = get_or_create_setting('history/file', None)

# 中文全角
chinese_full_sign = ['。', '，', '、', '；', '：', '？', '！', '“', '”', '‘', '’',
                     '（', '）', '…', '—', '《', '》', '〈', '〉', '·']

# 英文-中文 全角对应
chinese_full_dict = {
    # 中文空格去掉
    '([\u4e00-\u9fff])( )': '\g<1>',
    # 去掉多个/
    '(/)+': '\g<1>',
    ',': '，',
    '\.': '。',
    ':': '：',
    '!': '！',
    '\?': '？',
    '"': '“',
    "'": '‘',
    '\t': '',
    '\n': '',
}

q_st.sync()


if __name__ == '__main__':
    print(USER_HOME, DOCUMENT_DIR)
    print(q_st.value('theme/font-size', 'aaa'))
    print(q_st.fileName())
