"""
des: 中文 英文label
author: mr52hz
date: 2021-11-01
"""
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QResizeEvent
from PyQt5.QtWidgets import QLabel, QSizePolicy

from utils import cal_pinyin_width


class MyLabel(QLabel):
    # 自定义事件的标签
    onClicked = pyqtSignal(object)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.pressed = 0

    def mousePressEvent(self, event):
        self.pressed = 1

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton and self.pressed == 1:
            self.pressed = 0
            self.onClicked.emit(self)


class PinyinLabel(MyLabel):

    # (width, old_width)
    onPinyinWidthChanged = pyqtSignal(int, int)

    def __init__(self, pinyin: list, index: int, parent=None):
        super(PinyinLabel, self).__init__(parent)
        self.index = index
        self.setProperty('type', 'pinyin')
        self.initLabel(pinyin)
        self.adjustColorLevel(pinyin)

    def initLabel(self, pinyin):
        self.setAlignment(Qt.AlignCenter)
        # 拉伸策略 Preferred可以拉伸以Min为最小值，Fixed固定大小
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setText(pinyin[0])
        w = cal_pinyin_width(pinyin[0])
        self.setFixedSize(w, 25)

    def adjustColorLevel(self, pinyin):
        if len(pinyin) == 1:
            # 单音无需更改样式 默认黑色
            self.setProperty('level', 'normal')
        elif pinyin[-1] == 'focus':
            # 配置文件中重点注意的汉字 更改为绿色
            self.setProperty('level', 'focus')
        else:
            # 汉字多音 更改为红色
            self.setProperty('level', 'warning')

    def resizeEvent(self, a0: QResizeEvent) -> None:
        self.onPinyinWidthChanged.emit(self.width(), a0.oldSize().width())


class ZhLabel(MyLabel):

    def __init__(self, zh_char, width, parent=None):
        """
        中文
        :param zh_char: 中文字符
        :param width: label宽度需要和拼音保持一致
        :param parent: None
        """
        super(ZhLabel, self).__init__(parent)
        self.setProperty('type', 'zh')
        self.initLabel(zh_char, width)

    def initLabel(self, zh_char, width):
        self.setAlignment(Qt.AlignCenter)
        # 拉伸策略 Preferred可以拉伸以Min为最小值，Fixed固定大小
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setText(zh_char)
        self.setFixedSize(width, 25)

    def adjustWidth(self, width, *args):
        self.setFixedWidth(width)


