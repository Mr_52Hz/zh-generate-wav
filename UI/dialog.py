"""
des: 中文 拼音 修改界面
author: mr52hz
date: 2021-11-01
"""
import os

from PyQt5.QtCore import Qt, QThread, pyqtSignal
from PyQt5.QtGui import QKeyEvent, QMovie, QCloseEvent, QIcon
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QTextEdit, QHBoxLayout, QPushButton, QLabel, QInputDialog, \
    QMessageBox, QGridLayout, QComboBox, QListView, QApplication

import settings
from UI.task import GenerateWavDelayTask


class CannotRunTipMsg(QMessageBox):

    def __init__(self, parent=None, *args, **kwargs):
        self.icon_path = kwargs.pop('icon', settings.BASE_DIR + '/UI/qss/source/icon.ico')
        super(CannotRunTipMsg, self).__init__(parent, *args, **kwargs)
        self.status = False
        self._parent = parent
        self.initUi()

    def initUi(self):
        self.setMinimumSize(800, 400)
        self.setWindowIcon(QIcon(self.icon_path))
        self.setWindowTitle('警告')
        self.setText('软件已经在运行，禁止重复打开！')
        self.setIcon(QMessageBox.Warning)
        self.addButton("确定", QMessageBox.YesRole)

    def showTip(self):
        self.exec()


class CannotOpenEmptyTxtMsg(QMessageBox):

    def __init__(self, parent=None, *args, **kwargs):
        self.icon_path = kwargs.pop('icon', settings.BASE_DIR + '/UI/qss/source/icon.ico')
        super(CannotOpenEmptyTxtMsg, self).__init__(parent, *args, **kwargs)
        self.status = False
        self._parent = parent
        self.initUi()

    def initUi(self):
        self.setMinimumSize(800, 400)
        self.setWindowIcon(QIcon(self.icon_path))
        self.setWindowTitle('警告')
        self.setText('请打开合法txt文件！')
        self.setIcon(QMessageBox.Warning)
        self.addButton("确定", QMessageBox.YesRole)

    def showTip(self):
        self.exec()


class ZhEditDialog(QDialog):

    def __init__(self, parent=None, *args, **kwargs):
        super(ZhEditDialog, self).__init__(parent, *args, **kwargs)
        self.status = False
        self._parent = parent
        self.initUi()

    def initUi(self):
        self.setModal(True)
        self.setMinimumSize(800, 400)
        v_layout = QVBoxLayout()
        self.tip_label = QLabel(self)
        self.text_eidt = ZhTextEdit(self._parent)
        v_layout.addWidget(self.tip_label)
        v_layout.addWidget(self.text_eidt)

        btn_layout = QHBoxLayout()
        ok_btn = QPushButton('确定')
        ok_btn.clicked.connect(self.okPressedEvent)
        cancel_btn = QPushButton('取消')
        cancel_btn.clicked.connect(self.cancelPressEvent)
        btn_layout.addWidget(ok_btn)
        btn_layout.addWidget(cancel_btn)

        v_layout.addLayout(btn_layout)
        self.setLayout(v_layout)

    def okPressedEvent(self):
        self.status = True
        self.close()

    def cancelPressEvent(self):
        self.status = False
        self.close()

    def getMultiLineText(self, title, tip, text=''):
        self.setWindowTitle(title)
        self.tip_label.setText(tip)
        self.text_eidt.setText(text)
        self.text_eidt.setFocus()
        self.exec()
        if self.status is True:
            if self.text_eidt.toPlainText() == text:
                return 'text not change', False
            return self.text_eidt.toPlainText(), self.status
        else:
            return 'cancel', self.status


class ZhTextEdit(QTextEdit):

    def __init__(self, parent=None, *args):
        super().__init__(parent, *args)
        self._parent = parent
        self.setFocus()

    def keyPressEvent(self, e: QKeyEvent) -> None:
        if e.key() == Qt.Key_Return:
            self.parent().okPressedEvent()

        elif e.key() == Qt.Key_Space:
            # 实现播放暂停
            line = self._parent.lines_area.currentLine
            if self._parent.wav_player.isPlaying():
                line.stop()
            else:
                line.play()

        else:
            print(e.key(), '...')
            super(ZhTextEdit, self).keyPressEvent(e)


class PinYinEditDialog(QInputDialog):

    def __init__(self, parent=None):
        super(PinYinEditDialog, self).__init__(parent)

    def getNewPinyinStr(self, text):
        input_title = '修改拼音'
        input_tip = '数字1-5代表声调:'
        return super(PinYinEditDialog, self).getText(self.parent(), input_title, input_tip, text=text)


class RequestProgressDialog(QDialog):

    def __init__(self, parent, *args, **kwargs):
        super(RequestProgressDialog, self).__init__(parent, *args, **kwargs)
        self._parent = parent
        self.status = False
        self.initUi()
        self.initThread()

    def initUi(self):
        self.setModal(True)
        self.setMinimumSize(400, 200)
        layout = QVBoxLayout()
        self.wav_label = QLabel(self)
        self.status_label = QLabel(self)
        self.save_label = QLabel(self)

        # btn
        btn_layout = QHBoxLayout()
        btn_layout.setAlignment(Qt.AlignRight)
        self.ok_btn = QPushButton('确定')
        self.ok_btn.setFixedWidth(80)
        self.ok_btn.setDisabled(True)
        self.ok_btn.clicked.connect(self.okPressedEvent)
        cancel_btn = QPushButton('取消')
        cancel_btn.setFixedWidth(80)
        cancel_btn.clicked.connect(self.cancelPressEvent)

        layout.addWidget(self.wav_label)
        layout.addWidget(self.status_label)
        layout.addWidget(self.save_label)
        btn_layout.addWidget(self.ok_btn)
        btn_layout.addWidget(cancel_btn)

        layout.addLayout(btn_layout)
        self.setLayout(layout)

    def initThread(self):
        self.thread = QThread()
        self.generate_wav_delay_task = GenerateWavDelayTask(temp_path=self._parent.parser.wavs_temp)
        self.generate_wav_delay_task.moveToThread(self.thread)
        self.generate_wav_delay_task.startGenerateTask.connect(self.generate_wav_delay_task.generateWav)
        self.generate_wav_delay_task.onResponseReceived.connect(self.responseEvent)
        self.generate_wav_delay_task.onWavGenerated.connect(self.wavSavedEvent)
        self.thread.start()

    def responseEvent(self, p):
        status, time = p
        self.status_label.setText('请求完成! \nstatus: %s, time: %s' % (status, time))
        if status == 'OK':
            self.status = True
            self.ok_btn.setEnabled(True)
            self.ok_btn.setFocus(True)

    def wavSavedEvent(self, p):
        self.save_label.setText(p)

    def okPressedEvent(self):
        self.status = True
        self.close()

    def cancelPressEvent(self):
        self.status = False
        self.close()

    def closeEvent(self, a0: QCloseEvent) -> None:
        # 务必在关闭窗口之前 关掉多线程
        self.thread.terminate()
        super(RequestProgressDialog, self).closeEvent(a0)

    def startGenerateWav(self, command='step_1', sentences=None, names=None):
        if sentences is None or names is None or len(sentences) != len(names):
            return

        self.setWindowTitle('wav生成')
        self.wav_label.setText('wav num: %d' % len(sentences))

        gif = QMovie(self._parent.loading_gif)
        self.status_label.setMovie(gif)
        gif.start()

        self.save_label.setText('...')
        self.generate_wav_delay_task.startGenerateTask.emit(command, sentences, names)
        self.exec()

        return self.status


class MergeProgressDialog(QDialog):

    def __init__(self, parent=None, *args, **kwargs):
        self._parent = parent
        super(MergeProgressDialog, self).__init__(parent, *args, **kwargs)
        self.initUi()
        self.initThread()

    def initUi(self):
        self.setModal(True)
        self.setMinimumSize(400, 200)
        layout = QVBoxLayout()
        self.wav_label = QLabel(self)
        self.status_label = QLabel(self)

        # btn
        btn_layout = QHBoxLayout()
        btn_layout.setAlignment(Qt.AlignRight)
        self.ok_btn = QPushButton('确定')
        self.ok_btn.setFixedWidth(80)
        self.ok_btn.setDisabled(True)
        self.ok_btn.clicked.connect(self.okPressedEvent)

        layout.addWidget(self.wav_label)
        layout.addWidget(self.status_label)
        btn_layout.addWidget(self.ok_btn)

        layout.addLayout(btn_layout)
        self.setLayout(layout)

    def initThread(self):
        self.thread = QThread()
        self.generate_wav_delay_task = GenerateWavDelayTask(temp_path=self._parent.parser.root)
        self.generate_wav_delay_task.moveToThread(self.thread)
        self.generate_wav_delay_task.startMergeExportTask.connect(self.generate_wav_delay_task.mergeAndExportWav)
        self.generate_wav_delay_task.onWavMerged.connect(self.wavMergedEvent)
        self.generate_wav_delay_task.onWavExported.connect(self.wavExportedEvent)
        self.thread.start()

    def startMergeAndExportWav(self, ):

        self.setWindowTitle('wav合并')

        name = self._parent.parser.file_name[:-4] + '.wav'
        lines = self._parent.lines_area.lines
        wavs = []
        for line in lines:
            line_interval = line.duration_combo.currentText()
            wav_path = os.path.join(self._parent.parser.wavs_temp, str(line.index)+'.wav')
            wavs.append((line_interval, wav_path))

        self.wav_label.setText('wav num: %d' % len(wavs))

        if len(wavs) > 0:
            # wavs.sort(key=lambda x: x[1])
            # interval name wavs
            self.generate_wav_delay_task.startMergeExportTask.emit(name, wavs)

        self.exec()
        return True

    def wavMergedEvent(self, p):
        self.status_label.setText(p)

    def wavExportedEvent(self, p):
        self.status_label.setText(p)
        self.ok_btn.setEnabled(True)

    def okPressedEvent(self):
        self.close()

    def closeEvent(self, a0: QCloseEvent) -> None:
        self.thread.terminate()
        super(MergeProgressDialog, self).closeEvent(a0)


class QuickEditCombo(QComboBox):

    onEditTextSelected = pyqtSignal(bool)

    def __init__(self, parent=None, name=None):
        self.name = name
        super(QuickEditCombo, self).__init__(parent)
        self.setView(QListView())
        self.currentTextChanged.connect(self.isTextSelectLegal)

    def config(self, items, currentIndex=0, width=60):
        self.setFocusPolicy(Qt.NoFocus)
        self.setFixedWidth(width)
        self.addItem('默认')
        self.addItems(items)
        if self.count() == 1:
            self.setDisabled(True)
        self.setCurrentIndex(currentIndex)

    def next(self):
        if self.isEnabled():
            item_nums = self.count()
            next_index = (self.currentIndex() + 1) % item_nums
            self.setCurrentIndex(next_index)
            self.comboTextChange(self.currentText())

    def setDisabled(self, a0: bool) -> None:
        if self.count() == 1 and a0 is False:
            # 一个项目无法激活
            return
        super(QuickEditCombo, self).setDisabled(a0)

    def setEnabled(self, a0: bool) -> None:
        if self.count() == 1 and a0 is True:
            return
        super(QuickEditCombo, self).setEnabled(a0)

    def isTextSelectLegal(self, text):
        if text == '默认':
            self.onEditTextSelected.emit(False)
        else:
            self.onEditTextSelected.emit(True)


class QuickEditDialog(QDialog):

    def __init__(self, parent=None, *args, **kwargs):
        self._parent = parent
        self._is_adjusting = False
        super(QuickEditDialog, self).__init__(parent, *args, **kwargs)
        self.initUi()

    def initUi(self):
        self.setModal(True)
        self.setMinimumSize(400, 200)
        layout = QVBoxLayout()

        # combo box
        combo_box_layout = QGridLayout()
        combo_box_layout.setSpacing(10)
        combo_box_layout.setContentsMargins(0, 0, 0, 0)

        self.who_combo = QuickEditCombo()
        self.who_combo.config(settings.who)
        self.who_combo.onEditTextSelected.connect(self.changeAdjustBtnStatus)

        self.duration_combo = QuickEditCombo()
        self.duration_combo.config(settings.wav_quiet_durations)
        self.duration_combo.onEditTextSelected.connect(self.changeAdjustBtnStatus)

        self.emotion_combo = QuickEditCombo()
        self.emotion_combo.config(settings.emotions)
        self.emotion_combo.onEditTextSelected.connect(self.changeAdjustBtnStatus)

        self.color_combo = QuickEditCombo()
        self.color_combo.config(settings.colors)
        self.color_combo.onEditTextSelected.connect(self.changeAdjustBtnStatus)

        combo_box_layout.addLayout(self._getComboWithTipLayout('角 色：', self.who_combo), 0, 0)
        combo_box_layout.addLayout(self._getComboWithTipLayout('间 隔：', self.duration_combo), 0, 1)
        combo_box_layout.addLayout(self._getComboWithTipLayout('音 色：', self.color_combo), 1, 0)
        combo_box_layout.addLayout(self._getComboWithTipLayout('感 情：', self.emotion_combo), 1, 1)

        # btn
        btn_layout = QHBoxLayout()
        btn_layout.setAlignment(Qt.AlignRight)
        self.adjust_btn = QPushButton('调整')
        self.adjust_btn.setFocusPolicy(Qt.NoFocus)
        self.adjust_btn.setFixedWidth(80)
        self.adjust_btn.setDisabled(True)
        self.adjust_btn.clicked.connect(self.adjustPressedEvent)
        self.edit_progress_label = QLabel('%s/%s' % (0, self._parent.lines_area.unlockedNums))
        btn_layout.addWidget(self.edit_progress_label)
        btn_layout.addWidget(self.adjust_btn)

        layout.addWidget(QLabel(' '))
        layout.addLayout(combo_box_layout)
        layout.addWidget(QLabel(' '))
        layout.addLayout(btn_layout)
        self.setLayout(layout)

    def quickEditShow(self):
        self.setWindowTitle('快速调整')
        self.exec()

    def changeAdjustBtnStatus(self, status):
        if status is True:
            self.adjust_btn.setEnabled(True)
            if self.adjust_btn.text() != '调整':
                self.adjust_btn.setText('调整')
                self.edit_progress_label.setText('%s/%s' % (0, self._parent.lines_area.unlockedNums))
        else:
            if self._isAllDefault():
                self.adjust_btn.setDisabled(True)

    def adjustPressedEvent(self):
        if self.adjust_btn.text() == '调整':
            # 执行调整
            if self._is_adjusting is True:
                return

            self._is_adjusting = True

            who = self.who_combo.currentText()
            duration = self.duration_combo.currentText()
            emotion = self.emotion_combo.currentText()
            color = self.color_combo.currentText()

            edit_nums = 0
            for line in self._parent.lines_area.lines:

                if line.isLocked:
                    continue

                edit_nums += 1

                self.edit_progress_label.setText('%s/%s' % (edit_nums, self._parent.lines_area.unlockedNums))

                # 实时 刷新界面进度
                QApplication.processEvents()

                # 为了保持当前行 不因状态变 改变 断开
                line.onLineSelected.disconnect(self._parent.lines_area.toCurrentLine)

                self._quickChangeText(who, line.who_combo)
                self._quickChangeText(duration, line.duration_combo)
                self._quickChangeText(emotion, line.emotion_combo)
                self._quickChangeText(color, line.color_combo)

                # 恢复行状态改变选中当前行信号
                line.onLineSelected.connect(self._parent.lines_area.toCurrentLine)

            self._is_adjusting = False
            self.adjust_btn.setText('OK')
        else:
            self.close()

    def closeEvent(self, a0: QCloseEvent) -> None:
        super(QuickEditDialog, self).closeEvent(a0)

    def _isAllDefault(self):
        if self.who_combo.currentText() == '默认':
            if self.duration_combo.currentText() == '默认':
                if self.color_combo.currentText() == '默认':
                    if self.emotion_combo.currentText() == '默认':
                        return True
        return False

    @staticmethod
    def _getComboWithTipLayout(tip, combo):
        lt = QHBoxLayout()
        label = QLabel(tip)
        label.setFixedWidth(40)
        lt.addWidget(label)
        lt.addWidget(combo)
        return lt

    @staticmethod
    def _quickChangeText(text, combo):
        if text != '默认':
            if combo.isEnabled() and text != combo.currentText():
                combo.setCurrentText(text)
