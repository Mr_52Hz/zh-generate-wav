"""
des: 耗时任务 如： wav生成
author: mr52hz
date: 2021-11-04
"""
import json
import os

import numpy as np
import requests
from PyQt5.QtCore import QObject, pyqtSignal
from scipy.io import wavfile

import settings


class GenerateWavDelayTask(QObject):

    # 用于 外部启动 generateWav
    startGenerateTask = pyqtSignal(str, object, object)
    # interval name wavs [(interval, wav), ...]
    startMergeExportTask = pyqtSignal(str, object)

    # 向外部传递参数
    onResponseReceived = pyqtSignal(object)
    onWavGenerated = pyqtSignal(str)
    onWavMerged = pyqtSignal(str)
    onWavExported = pyqtSignal(str)

    def __init__(self, temp_path=None):
        super(GenerateWavDelayTask, self).__init__()
        self.temp_path = temp_path
        self.url = settings.generate_url
        self.data = None

    def generateWav(self, command='step_1', sentences=None, names=None):
        if sentences is None or names is None or len(sentences) != len(names):
            return

        self.data = {
            'command': command,
            'sentences': sentences,
        }

        try:
            r = requests.post(self.url, json=self.data)
            result = json.loads(r.text)
            status = result['status']
            prcs_time = round(result['time'], 3) if status == 'OK' else 0

            if status == 'OK':
                wavs = result['wavs']
                mels = result['mels']
                wav_len = len(wavs)
                for i in range(wav_len):
                    wav = np.array(wavs[i])
                    mel = np.array(mels[i], dtype=np.float32)
                    name = str(names[i])+'.wav'
                    mel_name = str(names[i]) + '_mel'
                    wavfile.write(os.path.join(self.temp_path, name), 22050, wav.astype(np.int16))
                    np.save(os.path.join(self.temp_path, mel_name), mel)
                    self.onWavGenerated.emit(name + ' saved!')

                # wav_len = 0
                if wav_len == 0 and len(sentences) > 0:
                    self.onResponseReceived.emit(('error', 0))
                    self.onWavGenerated.emit('服务端音频生成错误，请联系管理员！')
                    return

                self.onWavGenerated.emit('%s wavs saved!' % wav_len)

            self.onResponseReceived.emit((status, prcs_time))

        except Exception as e:
            self.onResponseReceived.emit(('error', 0))
            self.onWavGenerated.emit(str(e))

    def mergeAndExportWav(self, name=None, wavs=None):
        # wavs [(interval, wav), ...]
        if wavs is None or name is None:
            return

        # Samples per second
        sps = 22050

        total_wav = np.arange(1)
        for interval, wav_path in wavs:
            # Duration
            duration_s = self._toFloat(interval, 0.5)
            waveform_quiet = np.arange(duration_s * sps) * 0
            wav_quiet_integers = np.int16(waveform_quiet)

            rate, wav_data = wavfile.read(wav_path)
            total_wav = np.concatenate((total_wav, wav_data, wav_quiet_integers))
            _, _name = os.path.split(wav_path)
            self.onWavMerged.emit(_name + ' merged ...')

        merged_wav_path = os.path.join(os.path.join(self.temp_path, name))
        wavfile.write(merged_wav_path, sps, total_wav.astype(np.int16))
        self.onWavExported.emit(name + ' export done!')

    @staticmethod
    def _toFloat(s, v):
        try:
            v = float(s)
        except Exception:
            pass
        return v
