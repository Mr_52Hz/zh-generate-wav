"""
des: 中文 - 拼音 对齐展示
author: mr52hz
date: 2021-10-30
"""
import os
import re

import sip
from PyQt5.QtCore import pyqtSignal, Qt, QUrl
from PyQt5.QtGui import QResizeEvent
from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QVBoxLayout, QGridLayout, QPushButton, QWidget, QMessageBox, \
    QComboBox, QListView, QMenu, QApplication

import settings
from UI.dialog import ZhEditDialog, PinYinEditDialog
from UI.label import ZhLabel, PinyinLabel
from text import Zh, Pinyin
from utils import cal_pinyin_width, line_num_replace


class LineGroup(QGroupBox):

    onWidthChanged = pyqtSignal(int)
    onLineSelected = pyqtSignal(object)
    onLineChecked = pyqtSignal(str, bool)
    onLineLocked = pyqtSignal(str, bool)

    def __init__(self, index, parent=None, isConnectSignal=False):
        """
        中文 拼音 分组 对齐 为一行
        :param index: 行号
        :param parent:
        """
        num = '第%s行' % (index + 1)
        super(LineGroup, self).__init__(num, parent)
        self._index = index
        self._parent = parent
        self._is_selected = False
        self._is_edited = False
        self._is_checked = False
        self._is_locked = False
        self.isDeleted = False
        self.setObjectName('line-' + str(index))
        self.setProperty('state', 'normal')
        self.parser = self._parent.parser
        self.initUi()
        if isConnectSignal is True:
            self.connectSignal()

    def initUi(self):
        self.setMaximumHeight(125)
        main_layout = QHBoxLayout()
        self.py_zh_widget = self.getZhAndPinYinWidget()
        self.btn_group = self.getButtonGroup()
        main_layout.addWidget(self.btn_group)
        main_layout.addWidget(self.py_zh_widget)
        self.setLayout(main_layout)

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        if self._index == value:
            return
        # 未重命名前 获取wav信息
        origin_wav = self.wav
        origin_mel = self.mel
        is_rename_wav = self.isLineWavAvailable

        # line index改变
        self._index = value

        # 重名wav
        target_mel = self.mel
        target_wav = self.wav
        if is_rename_wav is True:
            os.rename(origin_wav, target_wav)
            os.rename(origin_mel, target_mel)

        self.setObjectName('line-' + str(value))
        num = '第%s行' % (value + 1)
        self.setTitle(num)

    @property
    def wav(self):
        return os.path.join(self.parser.wavs_temp, str(self.index) + '.wav')

    @property
    def mel(self):
        return os.path.join(self.parser.wavs_temp, str(self.index) + '_mel.npy')

    @property
    def isEmpty(self):
        return self.zh.zhStr == '！空白行！'

    @property
    def isEdited(self):
        return self._is_edited

    @isEdited.setter
    def isEdited(self, value):
        if value is True:
            self._is_edited = True
            self._parent.cache.changeLineState(self.index, 'edit', True)
            self.state_btn.setProperty('state', 'edited')
            self.state_btn.setText('已修改')
            self.onLineChecked.emit('edited', True)
        self.state_btn.style().unpolish(self.state_btn)
        self.state_btn.style().polish(self.state_btn)

    @property
    def isLocked(self):
        return self._is_locked

    @isLocked.setter
    def isLocked(self, value):
        if value is True:
            # 锁定 line边框设置为绿色 中英 combo 不可更改
            if self.isPlayBtnAvailable is False or self.isEmpty:
                # 无播放资源 或 为空 不可锁定
                return
            self._is_locked = True
            self._parent.cache.changeLineState(self.index, 'lock', True)
            self.state_btn.setProperty('state', 'locked')
            self.setProperty('state', 'locked')
            self.py_zh_widget.setDisabled(True)
            self.who_combo.setDisabled(True)
            self.emotion_combo.setDisabled(True)
            self.color_combo.setDisabled(True)
            self.duration_combo.setDisabled(True)
            self.state_btn.setText('锁定')
            self.onLineLocked.emit('locked', True)
        else:
            self._is_edited = False
            self._is_locked = False
            self._parent.cache.changeLineState(self.index, 'lock', False)
            self._parent.cache.changeLineState(self.index, 'edit', False)
            self.state_btn.setText('初 始')
            self.state_btn.setProperty('state', 'normal')
            _state = 'selected' if self.isSelected else 'normal'
            self.setProperty('state', _state)
            self.py_zh_widget.setEnabled(True)
            self.who_combo.setEnabled(True)
            self.duration_combo.setEnabled(True)
            self.color_combo.setEnabled(True)
            self.emotion_combo.setEnabled(True)
            self.onLineLocked.emit('locked', False)
        self.style().unpolish(self)
        self.state_btn.style().unpolish(self.state_btn)
        self.style().polish(self)
        self.state_btn.style().polish(self.state_btn)

    @property
    def isTextChecked(self):
        return self._is_checked

    @isTextChecked.setter
    def isTextChecked(self, value):
        if value is True:
            self._is_checked = True
            self._parent.cache.changeLineState(self.index, 'check', True)
            self.state_btn.setProperty('state', 'checked')
            self.state_btn.setText('已校验')
            self.onLineChecked.emit('checked', True)
        else:
            self._is_checked = False
            self._parent.cache.changeLineState(self.index, 'check', False)
            self.state_btn.setProperty('state', 'normal')
            if self.state_btn.text() == '已校验':
                self.onLineChecked.emit('checked', False)
            self.state_btn.setText('待校验')
        self.state_btn.style().unpolish(self.state_btn)
        self.state_btn.style().polish(self.state_btn)

    @property
    def isSelected(self):
        return self._is_selected

    @isSelected.setter
    def isSelected(self, state):
        if state is True:
            self._is_selected = True
            self.setProperty('state', 'selected')
        else:
            self._is_selected = False
            _state = 'locked' if self.isLocked else 'normal'
            self.setProperty('state', _state)
            # 非选中 状态 需要停止当前行的播放行为
            self.stop()

        self.style().unpolish(self)
        self.style().polish(self)

    @property
    def isPlayBtnAvailable(self):
        return self.play_btn.isEnabled() and self.isLineWavAvailable and not self.isEmpty

    @property
    def isLineWavAvailable(self):
        return os.path.exists(self.wav)

    @property
    def zh(self):
        return self.parser.zh_lines[self.index]

    @zh.setter
    def zh(self, zh):
        if isinstance(zh, Zh):
            self.parser.zh_lines[self.index] = zh

    @property
    def pinyin(self):
        return self.parser.pinyin_lines[self.index]

    @pinyin.setter
    def pinyin(self, pinyin):
        if isinstance(pinyin, Pinyin):
            self.parser.pinyin_lines[self.index] = pinyin

    @property
    def contentTuple(self):
        emo_str = self.color_combo.currentText() + '/' + self.emotion_combo.currentText()
        who_str = self.who_combo.currentText()
        _w = settings.who_dic.get(who_str, None)
        who_str = who_str if _w is None else _w
        return who_str, emo_str, self.pinyin.pinyinStr

    def getZhAndPinYinWidget(self):
        widget = QWidget()
        layout = QVBoxLayout()
        layout.setSpacing(10)
        zh_layout = QHBoxLayout()
        py_layout = QHBoxLayout()
        total_py_width = 320
        for zh_char, py, idx in self.pinyin.zhAndPinYinCharIter:
            if not py or not zh_char or py[0] is ' ':
                continue

            py_label = PinyinLabel(pinyin=py, index=idx)
            total_py_width += py_label.width()
            if py[0] != '/':
                py_label.onClicked.connect(self.pinyinClickEvent)

            zh_label = ZhLabel(zh_char, width=py_label.width())
            zh_label.onClicked.connect(self.zhClickedEvent)

            # 拼音宽度改变 同时需要调整中文和lineGroup的宽度 也需要考虑edit_area是否需要变宽
            py_label.onPinyinWidthChanged.connect(zh_label.adjustWidth)
            py_label.onPinyinWidthChanged.connect(self.adjustLineWidth)

            zh_layout.addWidget(zh_label)
            py_layout.addWidget(py_label)

        zh_group, py_group = ZhLineGroup(), QGroupBox()
        zh_group.setFixedHeight(30)
        py_group.setFixedHeight(30)
        zh_group.setLayout(zh_layout)
        py_group.setLayout(py_layout)
        zh_group.onBoxClicked.connect(self.zhClickedEvent)

        layout.addWidget(py_group)
        layout.addWidget(zh_group)

        # 设置边距 左 上 右 下
        zh_layout.setContentsMargins(10, 0, 0, 0)
        py_layout.setContentsMargins(10, 0, 0, 0)
        # 控件边距为0
        zh_layout.setSpacing(0)
        py_layout.setSpacing(0)

        widget.setLayout(layout)
        self.setFixedWidth(total_py_width)
        return widget

    def getButtonGroup(self):
        btn_group = QGroupBox()
        layout = QGridLayout()
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        # if play:
        self.play_btn = QPushButton()
        self.play_btn.setObjectName('play-btn')
        self.play_btn.setFocusPolicy(Qt.NoFocus)
        self.play_btn.setFixedSize(60, 25)
        self.play_btn.setText('播 放')
        if self.isPlayBtnAvailable is False:
            self.play_btn.setDisabled(True)
        self.play_btn.clicked.connect(self.playClickedEvent)

        self.state_btn = QPushButton()
        self.state_btn.setObjectName('state-btn')
        self.state_btn.setProperty('state', 'normal')
        self.state_btn.setFocusPolicy(Qt.NoFocus)
        self.state_btn.clicked.connect(self.finishClickedEvent)
        self.state_btn.setFixedSize(60, 25)
        self.state_btn.setText('初 始')

        self.who_combo = ComboBox(name='who')
        self.who_combo.config(settings.who)

        self.emotion_combo = ComboBox(name='emotion')
        self.emotion_combo.config(settings.emotions)

        self.color_combo = ComboBox(name='color')
        self.color_combo.config(settings.colors)

        self.duration_combo = ComboBox(name='duration')
        self.duration_combo.config(settings.wav_quiet_durations)

        layout.addWidget(self.play_btn, 0, 0)
        layout.addWidget(self.state_btn, 1, 0)
        layout.addWidget(self.who_combo, 0, 1)
        layout.addWidget(self.color_combo, 1, 1)
        layout.addWidget(self.duration_combo, 0, 2)
        layout.addWidget(self.emotion_combo, 1, 2)

        btn_group.setLayout(layout)
        btn_group.setFixedWidth(200)
        return btn_group

    def finishClickedEvent(self):

        if self.index != self._parent.lines_area.currentIndex:
            self._parent.lines_area.toCurrentLine(self)

        # Mode LineMode WavMode
        if self._parent.lines_area.isLineEditMode:
            # 1、初始 ==> 已校验
            if self.isTextChecked is False:
                self.isTextChecked = True
                self._parent.lines_area.nextLine()
                return
            # 2、已校验 ==> 待校验
            if self.isTextChecked is True:
                self.isTextChecked = False
                return
        else:
            # wavMode
            # 1、初始 ==> 锁定
            if self.isEdited is False and self.isLocked is False:
                self._parent.lines_area.nextLine()
                self.isLocked = True
                return
            # 2、锁定 ==> 初始
            if self.isLocked is True:
                self.isLocked = False
                return
            # 3、初始 ==> 已修改

    def playClickedEvent(self):
        self.onLineSelected.emit(self)
        if self._parent.wav_player.isPlaying() is False:
            self.play()
        else:
            self.stop()

    def zhClickedEvent(self):
        if self.isLocked:
            return
        self.onLineSelected.emit(self)
        content = self.zh.backslashStr
        # 汉语修改的Dialog
        input_ = ZhEditDialog(self._parent)
        input_title = '修改中文'
        input_tip = '修改Line: %s 汉字' % (self.index + 1)

        text, ok = input_.getMultiLineText(input_title, input_tip, text=content)
        if not text:
            text = content
        # 点击了OK并且内容发生了变化
        if ok is True:

            for key, value in settings.chinese_full_dict.items():
                text = re.sub(key, value, text)

            if bool(re.search(r'[0-9]', text)):
                # 0-9 替换掉
                text = line_num_replace(text)

            if content != text:
                if ''.join(text.split('/')) == self.zh.zhStr:
                    # 只是 更改了 分词 内容没有发生变化
                    self.zh = Zh(text)
                    self.pinyin.adjustBackslash(self.zh)
                else:
                    self.zh = Zh(text)
                    self.pinyin = Pinyin(self.zh)

                self._parent.cache.changeLineContent(self.index, self.zh, self.pinyin)
                self.rebuildZhPinWidget()
                self.lineContentChanged()

    def wavGeneratedEvent(self):
        # 生成wav请求成功
        # 1、打开播放按钮
        if self.isLineWavAvailable is True:
            self.play_btn.setEnabled(True)
        # 2、初始化 未锁定 状态按钮 并 恢复 已修改 行标识
        if self.isLocked is False:
            self.state_btn.setText('初 始')
            self.state_btn.setProperty('state', 'normal')
            self._is_edited = False
            self._parent.cache.changeLineState(self.index, 'edit', False)
            self.state_btn.style().unpolish(self.state_btn)
            self.state_btn.style().polish(self.state_btn)
        QApplication.processEvents()

    def pinyinClickEvent(self, py_widget: PinyinLabel):
        self.onLineSelected.emit(self)
        pinyin_str = py_widget.text()
        input_ = PinYinEditDialog(self)
        text, ok = input_.getNewPinyinStr(pinyin_str)

        if ok and pinyin_str != text:
            pinyin, num = text[:-1], text[-1]
            if pinyin.isalpha() and num in ['1', '2', '3', '4', '5']:
                # 更新parser中pinyin obj
                self.pinyin.insert(py_widget.index, text)
                # 调整宽度
                w = cal_pinyin_width(text)
                py_widget.setFixedSize(w, 25)
                # 修改显示
                py_widget.setText(text)
                py_widget.update()
                self._parent.cache.changeLineContent(self.index, None, self.pinyin)
                self.lineContentChanged()
            else:
                msg_box = QMessageBox(QMessageBox.Warning, '拼音错误', '只能包含a-z + 1-5')
                msg_box.exec_()

    def rebuildZhPinWidget(self):
        self.py_zh_widget.deleteLater()
        sip.delete(self.py_zh_widget)
        self.py_zh_widget = self.getZhAndPinYinWidget()
        self.layout().addWidget(self.py_zh_widget)

    def contextMenuEvent(self, event):
        if self.isLocked is True:
            return

        right_click_menu = QMenu(self)
        add_empty_line = right_click_menu.addAction("新建空白行")
        delete_current_line = right_click_menu.addAction("删除当前行")
        action = right_click_menu.exec_(self.mapToGlobal(event.pos()))

        if action == add_empty_line:
            self._parent.lines_area.insertEmptyLineBehindLine(self)
        elif action == delete_current_line:
            self._parent.lines_area.deleteLineWidget(self)

    def lineContentChanged(self):
        # 切换到 当前行
        self.onLineSelected.emit(self)
        # 状态调整
        if self._parent.lines_area.isLineEditMode:
            # 已校验 ==> 待校验
            self.isTextChecked = False
        else:
            # 初始 ==> 已修改
            self.isEdited = True

    def comboTextChangedEvent(self, key, value):
        self._parent.cache.changeLineState(self.index, key, value)

    def adjustLineWidth(self, width, old_width) -> None:
        if width != old_width and old_width > 0:
            w = self.width() + width - old_width
            self.setFixedWidth(w)

    def resizeEvent(self, a0: QResizeEvent) -> None:
        self.onWidthChanged.emit(a0.size().width())

    def play(self):
        if self.isPlayBtnAvailable is True:
            # setResource 不替换相同路径wav 需要先置为空
            self._parent.wav_player.setSource(QUrl.fromLocalFile(''))
            self._parent.wav_player.setSource(QUrl.fromLocalFile(self.wav))
            self._parent.wav_player.play()
            self.play_btn.setText('停 止')

    def stop(self):
        if self._parent.wav_player.isPlaying():
            self._parent.wav_player.stop()
            self.play_btn.setText('播 放')

    def connectSignal(self):
        self.who_combo.currentIndexChanged.connect(self.lineContentChanged)
        self.who_combo.currentTextChanged.connect(self.who_combo.comboTextChange)
        self.who_combo.onTextChanged.connect(self.comboTextChangedEvent)

        self.emotion_combo.currentIndexChanged.connect(self.lineContentChanged)
        self.emotion_combo.currentTextChanged.connect(self.emotion_combo.comboTextChange)
        self.emotion_combo.onTextChanged.connect(self.comboTextChangedEvent)

        self.color_combo.currentIndexChanged.connect(self.lineContentChanged)
        self.color_combo.currentTextChanged.connect(self.color_combo.comboTextChange)
        self.color_combo.onTextChanged.connect(self.comboTextChangedEvent)

        # 间隔选择 只对导出有影响 不视为行内容变化
        # self.duration_combo.currentIndexChanged.connect(self.lineContentChanged)
        self.duration_combo.onTextChanged.connect(self.comboTextChangedEvent)
        self.duration_combo.currentTextChanged.connect(self.duration_combo.comboTextChange)

        self.onWidthChanged.connect(self._parent.lines_area.adjustWidth)
        self.onLineSelected.connect(self._parent.lines_area.toCurrentLine)
        self.onLineChecked.connect(self._parent.lines_area.lineStateChangedEvent)
        self.onLineLocked.connect(self._parent.lines_area.lineStateChangedEvent)
        self._parent.control_area.onWavGenerateSuccess.connect(self.wavGeneratedEvent)

    def disconnectSignal(self):
        self.disconnect()
        self.duration_combo.disconnect()
        self.emotion_combo.disconnect()
        self.who_combo.disconnect()
        self.color_combo.disconnect()

    def __str__(self):
        return self.objectName()

    def __repr__(self):
        return self.objectName()


class ZhLineGroup(QGroupBox):
    # 自定义事件的标签
    onBoxClicked = pyqtSignal(object)

    def __init__(self):
        super(ZhLineGroup, self).__init__()
        self.pressed = 0

    def mousePressEvent(self, event):
        self.pressed = 1

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton and self.pressed == 1:
            self.pressed = 0
            self.onBoxClicked.emit(self)


class ComboBox(QComboBox):

    onTextChanged = pyqtSignal(str, str)

    def __init__(self, parent=None, name=None):
        self.name = name
        super(ComboBox, self).__init__(parent)
        self.currentTextChanged.connect(self.comboTextChange)
        self.setView(QListView())

    def config(self, items, currentIndex=0, width=60, name=None):
        if name is not None:
            self.name = name
        self.setFocusPolicy(Qt.NoFocus)
        self.setFixedWidth(width)
        self.addItems(items)
        if self.count() == 1:
            self.setDisabled(True)
        self.setCurrentIndex(currentIndex)

    def next(self):
        if self.isEnabled():
            item_nums = self.count()
            next_index = (self.currentIndex() + 1) % item_nums
            self.setCurrentIndex(next_index)
            self.comboTextChange(self.currentText())

    def comboTextChange(self, value):
        self.onTextChanged.emit(self.name, value)

    def setDisabled(self, a0: bool) -> None:
        if self.count() == 1 and a0 is False:
            # 一个项目无法激活
            return
        super(ComboBox, self).setDisabled(a0)

    def setEnabled(self, a0: bool) -> None:
        if self.count() == 1 and a0 is True:
            return
        super(ComboBox, self).setEnabled(a0)


