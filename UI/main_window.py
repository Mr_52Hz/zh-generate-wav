"""
拼音->wav
author: Mr_52Hz
date: 2019-7-3
"""
import os

from PyQt5.QtCore import QCoreApplication, Qt, pyqtSignal, QUrl
from PyQt5.QtMultimedia import QSoundEffect
from PyQt5.QtWidgets import QMainWindow, QWidget, QAction, qApp, QFileDialog, QVBoxLayout, \
    QScrollArea, QApplication, QHBoxLayout, QPushButton
from PyQt5.QtGui import QIcon, QFont, QKeyEvent, QDesktopServices, QCloseEvent, QMouseEvent
import sys

import settings
from UI.dialog import RequestProgressDialog, MergeProgressDialog, QuickEditDialog, CannotOpenEmptyTxtMsg
from UI.line import LineGroup
from text import TxtParser, CsvParser, Cache


class Window(QMainWindow):
    """

    """

    def __init__(self, *args, **kwargs):
        qss_f = kwargs.pop('qss', settings.BASE_DIR + '/UI/qss/main.qss')
        self.icon_path = kwargs.pop('icon', settings.BASE_DIR + '/UI/qss/source/icon.ico')
        self.loading_gif = kwargs.pop('gif', settings.BASE_DIR + '/UI/qss/source/loading.gif')
        super().__init__(*args, **kwargs)
        self.is_opened = False
        self.parser = None
        self.cache = None   # type: Cache

        self.scroll_area = None     # type: QScrollArea
        self.wav_player = QSoundEffect()
        self.wav_player.setLoopCount(-2)
        self.wav_player.setVolume(1.0)
        # 布局
        self.initUi()

        with open(qss_f, 'r', encoding='utf-8') as f:
            qApp.setStyleSheet(f.read())

    def initUi(self):
        """
        初始化UI界面参数
        :return: None
        """
        # 加载qss样式配置文件
        # with open(config.BASE_DIR + '/UI/qss/main_interface.qss', 'r', encoding='UTF-8') as f:
        #     qApp.setStyleSheet(f.read())

        # 创建状态栏
        self.status_bar = self.statusBar()
        self.status_bar.showMessage('%s-design by Mr52Hz' % settings.LOG_VERSION)

        # 一系列菜单栏 Action
        # 打开
        open_act = QAction(QIcon(), '打开(&O)', self)
        open_act.setShortcut('Ctrl+O')
        open_act.setStatusTip('打开txt文本文件')
        open_act.triggered.connect(self.openFileMenu)

        # 关闭
        close_act = QAction(QIcon(), '关闭(&W)', self)
        close_act.setShortcut('Ctrl+W')
        close_act.setStatusTip('关闭工作区')
        close_act.triggered.connect(self.closeFileMenu)

        # 设置
        config_act = QAction(QIcon(), '设置(&F)', self)
        config_act.setShortcut('Ctrl+F')
        config_act.setStatusTip('打开config-%s.ini设置目录' % settings.LOG_VERSION)
        config_act.triggered.connect(self.openConfigDir)

        # show in Exp
        show_in_explorer = QAction(QIcon(), '显示(&E)', self)
        show_in_explorer.setShortcut('Ctrl+E')
        show_in_explorer.setStatusTip('在资源管理器中打开')
        show_in_explorer.triggered.connect(self.showInExplorer)

        # 退出动作
        exit_act = QAction(QIcon(), '退出(&E)', self)
        exit_act.setShortcut('Ctrl+Q')
        exit_act.setStatusTip('退出程序')
        exit_act.triggered.connect(self.quit)

        # 保存
        save_act = QAction(QIcon(), '保存(&S)', self)
        save_act.setShortcut('Ctrl+S')
        save_act.setStatusTip('保存')
        save_act.triggered.connect(self.saveFileMenu)

        # 快速调整
        quick_edit = QAction(QIcon(), '调整(&I)', self)
        quick_edit.setShortcut('Ctrl+I')
        quick_edit.setStatusTip('快速设置所有行信息')
        quick_edit.triggered.connect(self.quickEditLine)

        # 菜单栏
        self.menu_bar = self.menuBar()
        file_menu = self.menu_bar.addMenu('文件(&F)')
        edit_menu = self.menu_bar.addMenu('编辑(&E)')
        file_menu.addAction(open_act)
        file_menu.addAction(close_act)
        file_menu.addAction(config_act)
        file_menu.addAction(show_in_explorer)
        file_menu.addAction(exit_act)

        edit_menu.addAction(save_act)
        edit_menu.addAction(quick_edit)
        # 工作区
        self.work_area = QWidget(self)
        self.work_area.setMinimumSize(1000, 500)

        # 将work_area设置为主窗口
        self.setCentralWidget(self.work_area)

        self.setWindowTitle('语音合成')
        self.setWindowIcon(QIcon(self.icon_path))
        # 设置启动主窗口位置为(200, 200)处，并且宽为1000，高度为500
        self.setMinimumSize(1000, 550)
        if settings.theme_window_size is None:
            self.setGeometry(200, 200, 1000, 500)
        else:
            self.setGeometry(settings.theme_window_size)
        self.show()

    def initWorkArea(self):
        """
        初始化工作区 1、主窗口布局 2、行区域 3、控制区域
        :return:
        """
        # 设置主窗口布局
        self.work_area_lt = QVBoxLayout()
        self.work_area.setLayout(self.work_area_lt)

        # 编辑区
        self.lines_area = LinesAreaWidget(self)

        # 控制区
        self.control_area = ControlAreaWidget(self)

    def initScrollArea(self):
        # after initWorkArea And after insert lines to LinesArea
        self.scroll_area = QScrollArea()
        self.scroll_area.setFocusPolicy(Qt.NoFocus)
        self.scroll_area.verticalScrollBar().setMaximum(100)
        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scroll_area.horizontalScrollBar().setSingleStep(300)
        self.scroll_area.setWidget(self.lines_area)

        self.control_area.onWavGenerateSuccess.connect(self.lines_area.requestDoneEvent)
        self.lines_area.onAllLineLocked.connect(self.control_area.exportBtnStateChangedEvent)
        self.lines_area.onAllLineChecked.connect(self.control_area.requestBtnStateChangedEvent)
        self.lines_area.onLineNumChanged.connect(self.adjustWorkArea)

        self.work_area_lt.addWidget(self.scroll_area)
        self.work_area_lt.addWidget(self.control_area)

    def adjustWorkArea(self, lineLength):
        """
        行数量发生变化时，需要调整LinesArea大小和控制滚动条
        :param lineLength: 行数量
        :return:
        """
        # 重新计算编辑区的大小
        total_line_height = 125 * lineLength

        self.lines_area.setFixedHeight(total_line_height + 50)

    def rebuildWorkArea(self):
        pass

    def openFileMenu(self):
        # 打开文件 & 限制文件格式为docx 和 txt
        # 是否已经有代开的文档 如果有首先关闭
        if self.is_opened is True:
            return
            # self.closeFileMenu()

        default_path = settings.DESKTOP_DIR
        if settings.recent_path is not None and os.path.exists(settings.recent_path):
            default_path = settings.recent_path

        _extension = '*.txt *.csv'
        frame = QFileDialog.getOpenFileName(self, '打开文件', default_path, _extension)

        if len(frame) == 2 and frame[1] == _extension:
            path = frame[0]
            if path.endswith('txt'):
                self.parser = TxtParser(path)
            elif path.endswith('csv'):
                self.parser = CsvParser(path)
            self.cache = Cache(path)

            if self.parser.isEmpty:
                cannot = CannotOpenEmptyTxtMsg(icon=self.icon_path)
                cannot.showTip()
                return
            elif self.cache.hasCached:
                # 加载cache
                self.loadCache()
            else:
                # 解析文件
                self.parseFile()
            # 软件记录文件打开状态，保存最近操作记录
            self.is_opened = True
            settings.q_st.setValue('history/file', frame[0])
            settings.recent_path = frame[0]
            settings.q_st.sync()

    def saveFileMenu(self):
        if self.is_opened is False:
            return

        self.cache.dumps()

    def quickEditLine(self):
        if self.is_opened is False:
            return

        if self.lines_area.isAllLocked:
            return

        _quick = QuickEditDialog(self)
        _quick.quickEditShow()

    def closeFileMenu(self):

        if self.is_opened is False:
            return

        for w in self.work_area.children()[1:]:
            w.deleteLater()

        self.is_opened = False
        self.wav_player.stop()
        self.status_bar.showMessage('%s closed' % self.parser.file_name)

    def loadCache(self):
        self.cache.load()
        self.cache.isLocked = True

        self.initWorkArea()

        self.lines_area._lines_mode = self.cache.mode

        self.initScrollArea()
        self.wait(True)

        for index, zh_obj, py_obj, line_dict in self.cache.getZhAndPyOjbIter():

            self.parser.append(zh_obj, py_obj)

            self.status_bar.showMessage('%s/%s loading...' % (index+1, self.cache.height))

            line = self._createNewLineAndAddToLinesArea(index,
                                                        isChecked=line_dict[self.cache.is_checked_key],
                                                        isEdited=line_dict[self.cache.is_edited_key],
                                                        isLocked=line_dict[self.cache.is_locked_key])
            line.disconnectSignal()
            # 下拉框 恢复
            line.who_combo.setCurrentText(line_dict[self.cache.who_key])
            line.emotion_combo.setCurrentText(line_dict[self.cache.emotion_key])
            line.color_combo.setCurrentText(line_dict[self.cache.color_key])
            line.duration_combo.setCurrentText(line_dict[self.cache.duration_key])

            if line.index == self.cache.currentLineIndex:
                # 切换到当前行
                self.lines_area.toCurrentLine(line)

            # 后绑定信号
            line.connectSignal()
        # 处理状态 是否全锁定？
        self.lines_area.lineStateChangedEvent('', '')

        self.wait(False)
        self.adjustScrollBarPosition()
        # 缓存加载完成，初始化导出按钮状态
        self.lines_area.onAllLineLocked.emit(self.lines_area.isAllLocked)
        self.lines_area.setFocus()
        self.cache.isLocked = False

    def parseFile(self):
        # 解析
        self.parser.parse()

        self.initWorkArea()

        self.cache.mode = '分词校验'
        self.initScrollArea()
        self.wait(True)
        # 添加line
        for index in range(self.parser.height):
            line = self._createNewLineAndAddToLinesArea(index)
            self.status_bar.showMessage('%s/%s loading...' % (index+1, self.parser.height))
            self.cache.addLine(line)
        self.lines_area.toCurrentLine(self.lines_area.currentLine)
        self.cache.dumps()
        self.wait(False)
        self.lines_area.setFocus()
        self.status_bar.showMessage('%s 共%s行' % (self.parser.file_name, self.parser.height))

    def wait(self, status):
        # initWorkArea initScrollArea 必须等到这两个初始化才能执行
        if status is True:
            self.lines_area.setDisabled(True)
            self.control_area.setDisabled(True)
            self.menu_bar.setDisabled(True)
            self.status_bar.showMessage('loading...')
        else:
            self.lines_area.setEnabled(True)
            self.menu_bar.setEnabled(True)
            self.control_area.setEnabled(True)

    def _createNewLineAndAddToLinesArea(self, index, isChecked=False, isEdited=False, isLocked=False):
        """
        1、创建新的行
        2、设置行 状态
        3、添加到LinesArea
        :param index: 行号
        :param isChecked: 行编辑模式下 是否校验状态
        :param isEdited: wav校验模式下 是否编辑行 需要重新生成wav
        :param isLocked: wav校验下 是否 锁定
        :return: line<LineGroup>
        """
        # 1、新建一行
        line = LineGroup(index, self)

        # 2、设置 状态
        if self.lines_area.isLineEditMode:
            if isChecked is True:
                line.isTextChecked = True
        else:
            if isLocked is True:
                line.isLocked = True
            elif isEdited is True:
                line.isEdited = True

        # 3、添加到LinesArea
        line.move(0, index * 80)
        line.connectSignal()
        self.lines_area.appendLineWidget(line)
        QApplication.processEvents()
        return line

    def adjustScrollBarPosition(self):

        if self.lines_area.lineLength < 3:
            return

        percent = self.lines_area.currentIndex / (self.lines_area.lineLength - 1)
        bar_max = self.scroll_area.verticalScrollBar().maximum()

        self.scroll_area.verticalScrollBar().setValue(int(bar_max * percent))
        # 横向滚动条恢复 0点
        self.scroll_area.horizontalScrollBar().setValue(0)

    def showInExplorer(self):
        if self.is_opened is True:
            url = QUrl('file:///' + self.parser.root)
            QDesktopServices().openUrl(url)

    @staticmethod
    def openConfigDir():
        url = QUrl('file:///' + settings.APP_DIR)
        QDesktopServices().openUrl(url)

    def closeEvent(self, event: QCloseEvent) -> None:
        self.quit()

    def quit(self):
        settings.q_st.setValue('theme/window', self.geometry())
        # 显示调用关闭窗口
        self.close()
        # 等待写入等操作完成 quit
        qApp.quitOnLastWindowClosed()


class LinesAreaWidget(QWidget):

    onAllLineChecked = pyqtSignal(bool)
    onAllLineLocked = pyqtSignal(bool)
    onLineNumChanged = pyqtSignal(int)
    onLineMerged = pyqtSignal(int)

    def __init__(self, parent: Window = None, *args):
        super(LinesAreaWidget, self).__init__(parent, *args)
        # 编辑区因为滚动条导致父对象 变动
        self._parent = parent

        self.setObjectName('edit-area')
        self.setMinimumSize(950, 470)
        self.lt = QVBoxLayout()
        self.lt.setSpacing(10)
        self.setLayout(self.lt)

        self._current_line_index = 0
        self._total_line_nums = 0
        self._locked_nums = 0
        self._checked_nums = 0

        # 分词校验 / wav校验
        self._lines_mode = '分词校验'

    def appendLineWidget(self, line):
        if not isinstance(line, LineGroup):
            return

        self._total_line_nums += 1
        if line.isLocked:
            self._locked_nums += 1
        if line.isTextChecked:
            self._checked_nums += 1

        self.lt.addWidget(line)
        self.onLineNumChanged.emit(self.lineLength)

    def insertEmptyLineBehindLine(self, line):
        if not isinstance(line, LineGroup):
            return
        if line.isLocked is True:
            return
        new_line_index = line.index + 1
        self._parent.parser.insertEmptyZhPyObj(new_line_index)
        new_line = LineGroup(new_line_index, self._parent, isConnectSignal=True)

        # 1、先将行缓存
        self._parent.cache.insertLine(new_line)
        # 2、包含new_line位置的剩余行号 + 1
        for left_line in self.lines[:new_line_index-1:-1]:
            # 倒叙便于添加wav
            left_line.index += 1
        # 3、最后添加新行
        self.lt.insertWidget(new_line_index, new_line)
        if self._current_line_index > new_line_index:
            # 由于新插入一行 导致当前行 后移一行
            self._current_line_index += 1

        # 5、更新 计数+1 信号处理
        self._total_line_nums += 1
        self.onLineNumChanged.emit(self.lineLength)
        self.onAllLineLocked.emit(self.isAllLocked)
        self._setProgressMsg()

        # 切换到 新建行
        self.toCurrentLine(new_line, ensure=True)

    def deleteLineWidget(self, line):
        if not isinstance(line, LineGroup):
            return

        if line.isLocked is True:
            return

        if self.lineLength < 2:
            # 少于2行 不支持删除
            return

        if line.isLineWavAvailable:
            # 先wav
            os.remove(line.wav)
            os.remove(line.mel)
        # 1、缓存中删除行
        self._parent.cache.deleteLine(line)
        # 2、删除 解析器中的行
        self._parent.parser.pop(line.index)

        # 3、包含new_line位置的剩余行号 + 1
        for left_line in self.lines[line.index+1:]:
            left_line.index -= 1

        if self._current_line_index > line.index:
            # 由于删除一行 导致当前行 前移一行
            self._current_line_index -= 1

        # 4、从布局中删除
        _index = line.index
        line.isDeleted = True
        self.lt.removeWidget(line)
        line.deleteLater()

        # 5、更新
        self._total_line_nums -= 1

        # 切换到 新行
        self.toCurrentLine(self.getIndexLine(_index), ensure=True)

        self.onLineNumChanged.emit(self.lineLength)
        self.onAllLineLocked.emit(self.isAllLocked)
        self._setProgressMsg()

    def toCurrentLine(self, line: LineGroup, ensure=False):
        current_line = self.currentLine

        if current_line.index == line.index and line.index > 0 and ensure is False:
            # 大于第一行的 相同行 则不需要切换
            return
        elif current_line.index == line.index == 0 and ensure is False:
            line.isSelected = True
            return

        # 重置当前行
        current_line.isSelected = False

        # 切换到当前行
        self.currentIndex = line.index
        line.isSelected = True

    def nextLine(self, reverse=False):
        # 重置当前行 state
        line = self.currentLine
        if line.isSelected is True:
            # 选中下一行
            if reverse is True:
                self.currentIndex -= 1
            else:
                self.currentIndex += 1
            # 顶 底 不要调整
            if line.index == self.currentIndex:
                return
            # 状态重置
            line.isSelected = False
            line = self.currentLine
            line.isSelected = True
        # 滚动条 调整
        self._parent.adjustScrollBarPosition()

    def nextUnCheckedLine(self, reverse=False):
        # unchecked
        self._nextUnStatedLine(reverse=reverse, state='unchecked')

    def nextUnLockedLine(self, reverse=False):
        # unlocked
        self._nextUnStatedLine(reverse=reverse, state='unlocked')

    def _nextUnStatedLine(self, reverse=False, state='unlocked'):
        current_line = self.currentLine
        left_lines = self.leftLines(reverse=reverse)
        for line in left_lines:
            if (state == 'unlocked' and line.isLocked is False and line.index != current_line.index) \
                    or (state == 'unchecked' and line.isTextChecked is False and line.index != current_line.index):
                self.toCurrentLine(line)
                self._parent.adjustScrollBarPosition()
                break
        else:
            if len(left_lines) > 1:
                if (state == 'unlocked' and self.isAllLocked) \
                        or (state == 'unchecked' and self.isAllChecked):
                    # 如果都修改完成 则 跳转到 顶 或者 底
                    if reverse is True:
                        self.toCurrentLine(left_lines[0])
                    else:
                        self.toCurrentLine(left_lines[-1])
                    self._parent.adjustScrollBarPosition()

    @property
    def lines(self):
        # findChildren 照插入顺序返回
        # 即 后来insertWidget(index, widget)
        # index不在最后但是依然排在findChildren最后
        # 要 未删除的 行
        lines = [line for line in self.findChildren(LineGroup) if line.isDeleted is False]
        # 根据行 index排序
        lines.sort(key=lambda x: x.index)
        return lines

    @property
    def isAllChecked(self):
        return self._total_line_nums == self._checked_nums

    @property
    def isAllLocked(self):
        return self._total_line_nums == self._locked_nums

    @property
    def isLineEditMode(self):
        # 行 编辑 校验 模式 或者 wav生成 模式
        return self._lines_mode == '分词校验'

    def leftLines(self, reverse=False, line=None):
        current_line = self.currentLine if line is None else line
        lines = self.lines
        if reverse is True:
            left_lines = lines[:current_line.index]
        else:
            left_lines = lines[current_line.index:]
        return left_lines

    @property
    def lineLength(self):
        return self._total_line_nums

    @property
    def unlockedNums(self):
        return self._total_line_nums - self._locked_nums

    @property
    def currentIndex(self):
        return self._current_line_index

    @currentIndex.setter
    def currentIndex(self, value):
        if 0 <= value < self.lineLength:
            self._current_line_index = value
            self._parent.cache.currentLineIndex = value

    @property
    def currentLine(self) -> LineGroup:
        if self.currentIndex < self.lineLength:
            return self.lines[self.currentIndex]
        else:
            return self.lines[-1]

    def getIndexLine(self, index) -> LineGroup:
        if index < self.lineLength:
            return self.lines[index]
        else:
            return self.lines[-1]

    def _setProgressMsg(self):
        if self.isLineEditMode:
            msg = '%s 进度 # [checked: %s] #共%s行' % \
                  (self._lines_mode, self._checked_nums, self._total_line_nums,)
        else:
            msg = '%s 进度 # [locked: %s] #共%s行' % \
                  (self._lines_mode, self._locked_nums, self._total_line_nums,)
        self._parent.status_bar.showMessage(msg)

    def keyPressEvent(self, e: QKeyEvent) -> None:
        if e.key() == Qt.Key_Return:
            # 完成按钮
            self.currentLine.finishClickedEvent()

        elif e.key() == Qt.Key_N:
            if QApplication.keyboardModifiers() == Qt.ControlModifier:
                current_line = self.currentLine
                if current_line.isLocked is True:
                    return
                self.insertEmptyLineBehindLine(current_line)

        elif e.key() == Qt.Key_Delete:
            if QApplication.keyboardModifiers() == Qt.ControlModifier:
                current_line = self.currentLine
                if current_line.isLocked is True:
                    return
                self.deleteLineWidget(current_line)

        elif e.key() == Qt.Key_Down:
            if QApplication.keyboardModifiers() == Qt.ControlModifier:
                # ctrl+down
                if self.isLineEditMode:
                    self.nextUnCheckedLine()
                else:
                    self.nextUnLockedLine()
            else:
                # down
                self.nextLine()

        elif e.key() == Qt.Key_Up:
            if QApplication.keyboardModifiers() == Qt.ControlModifier:
                if self.isLineEditMode:
                    self.nextUnCheckedLine(reverse=True)
                else:
                    self.nextUnLockedLine(reverse=True)
            else:
                self.nextLine(reverse=True)

        elif e.key() == Qt.Key_Left:
            if QApplication.keyboardModifiers() == Qt.ControlModifier:
                self._parent.scroll_area.horizontalScrollBar().setValue(0)
            else:
                super(LinesAreaWidget, self).keyPressEvent(e)

        elif e.key() == Qt.Key_Right:
            if QApplication.keyboardModifiers() == Qt.ControlModifier:
                current_line = self.currentLine
                w = current_line.width() - 100
                self._parent.scroll_area.horizontalScrollBar().setValue(w)
            else:
                super(LinesAreaWidget, self).keyPressEvent(e)

        elif e.key() == Qt.Key_1:
            self.currentLine.who_combo.next()

        elif e.key() == Qt.Key_2:
            self.currentLine.duration_combo.next()

        elif e.key() == Qt.Key_3:
            self.currentLine.color_combo.next()

        elif e.key() == Qt.Key_4:
            self.currentLine.emotion_combo.next()

        elif e.key() == Qt.Key_Space:
            # 实现播放暂停
            line = self.currentLine
            if self._parent.wav_player.isPlaying():
                line.stop()
            else:
                line.play()

        elif e.key() == Qt.Key_Apostrophe:
            # ” 键打开汉字编辑区
            self.currentLine.zhClickedEvent()

        else:
            print(e.key(), '...')
            super(LinesAreaWidget, self).keyPressEvent(e)

    def mousePressEvent(self, a0: QMouseEvent) -> None:
        super(LinesAreaWidget, self).mousePressEvent(a0)
        self.setFocus()

    def adjustWidth(self, width):
        if width > self.width():
            self.setMinimumWidth(width)

    def lineStateChangedEvent(self, key, state):
        if key == 'checked':
            if state is True:
                self._checked_nums += 1
            else:
                self._checked_nums -= 1
        elif key == 'locked':
            if state is True:
                self._locked_nums += 1
            else:
                self._locked_nums -= 1

        self.onAllLineLocked.emit(self.isAllLocked)

        # is_all_checked = True if self._checked_nums == self._total_line_nums else False
        # if is_all_checked is False:
        #     self.all_line_checked.emit(is_all_checked)

        self._setProgressMsg()

    def requestDoneEvent(self):
        self.toCurrentLine(self.lines[0])
        self._parent.adjustScrollBarPosition()
        self._lines_mode = 'wav校验'
        self._parent.cache.mode = self._lines_mode
        self._setProgressMsg()


class ControlAreaWidget(QWidget):

    onWavGenerateSuccess = pyqtSignal()

    def __init__(self, parent: Window, *args):
        super(ControlAreaWidget, self).__init__(parent, *args)
        self._parent = parent
        self.initUi()

    def initUi(self):
        self.setMinimumWidth(950)
        self.setFixedHeight(50)
        layout = QHBoxLayout()
        layout.setAlignment(Qt.AlignRight)
        self.request_btn = QPushButton('生成')
        self.request_btn.setFocusPolicy(Qt.NoFocus)
        # self.request_btn.setDisabled(True)
        self.request_btn.setFixedSize(80, 40)
        self.request_btn.clicked.connect(self.requestEvent)

        self.export_btn = QPushButton('导出')
        self.export_btn.setFocusPolicy(Qt.NoFocus)
        self.export_btn.setDisabled(True)
        self.export_btn.setFixedSize(80, 40)
        self.export_btn.clicked.connect(self.exportEvent)

        layout.addWidget(self.request_btn)
        layout.addWidget(self.export_btn)

        self.setLayout(layout)

    def requestEvent(self):
        # 停止播放
        if self._parent.wav_player.isPlaying():
            current_line = self._parent.lines_area.currentLine
            current_line.stop()

        names, sentences = [], []
        for line in self._parent.lines_area.lines:
            # if line.isTextChecked is True and line.isLocked is False:
            if line.isLocked is False and not line.isEmpty:
                # 没锁定 即可请求
                sentences.append(line.contentTuple)
                names.append(line.index)
        _request = RequestProgressDialog(self._parent)
        status = _request.startGenerateWav(sentences=sentences, names=names)
        if status is True:
            self.onWavGenerateSuccess.emit()

    def exportEvent(self):
        # 停止播放
        if self._parent.wav_player.isPlaying():
            current_line = self._parent.lines_area.currentLine
            current_line.stop()

        _merge_export = MergeProgressDialog(self._parent)
        status = _merge_export.startMergeAndExportWav()

    def requestBtnStateChangedEvent(self, state):
        pass
        # if state is True:
        #     self.request_btn.setEnabled(True)
        # else:
        #     self.request_btn.setDisabled(True)

    def exportBtnStateChangedEvent(self, state):
        if state is True:
            self.export_btn.setEnabled(True)
            # self.request_btn.setDisabled(True)
        else:
            self.export_btn.setDisabled(True)
            # self.request_btn.setEnabled(True)


if __name__ == '__main__':
    QCoreApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
    app = QApplication(sys.argv)
    app_font = QFont("微软雅黑", 10)
    app.setFont(app_font)
    window = Window()
    sys.exit(app.exec())
