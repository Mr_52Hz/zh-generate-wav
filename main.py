"""
des: fbs 启动文件
author: mr52hz
date: 2021-11-11
"""
import sys
from PyQt5.QtCore import QCoreApplication, Qt, QSharedMemory
from fbs_runtime.application_context.PyQt5 import ApplicationContext

from UI import CannotRunTipMsg, Window


if __name__ == '__main__':

    # 适配更高分辨率
    QCoreApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
    appctxt = ApplicationContext()
    # fbs 获取qss 需要拷贝src/main/resources/base/
    # https://github.com/mherrmann/fbs-tutorial
    icon_path = appctxt.get_resource('icon.png')
    gif_path = appctxt.get_resource('loading.gif')
    main_qss = appctxt.get_resource('main.qss')
    app = appctxt.app

    share = QSharedMemory()
    share.setKey("main_window")

    if share.attach():
        cannot = CannotRunTipMsg(icon=icon_path)
        cannot.showTip()
        sys.exit(-1)
    if share.create(1):
        window = Window(qss=main_qss, icon=icon_path, gif=gif_path)
        sys.exit(app.exec())
