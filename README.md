# zhGenerateWav

## 介绍
`zhGenerateWav`软件是一个加载txt文本文件，具备断句->分词->拼音->wav生成的功能。

## 软件架构
Python+PyQt5+FBS+jieba+pypinyin+scipy

## 设计逻辑
#### 1、Zh对象 Pinyin对象
将每行文字和拼音封装为对象形式，拼音多音为绿色，重点汉字为红色。
#### 2、行区域（lines_area）
所有行所在区域，具备下一行，当前行等属性
#### 3、按钮
##### 3.1 行校验模式
`初始` 按钮-点击变成`已校验`           
`已校验`按钮-点击与`待校验`互相切换        
##### 3.2 wav校验模式
`初始` 按钮-点击与`锁定` 按钮相互切换      
`已修改`按钮-只有重新生成wav后，状态重置为`初始`        

## 快捷按键
`"` 引号键 : 打开中文编辑框       
`return` 回车键 : 待完成/已完成 按钮按下     
`space` 空格键 : 播放/暂停     
`L` 字母L : 锁定        
`1` 数字1 : 切换语音合成角色      
`2` 数字2 : 切换wav句子空白间隔         
`3` 数字3 : 切换感情色彩    
`Ctrl+up` Ctl+方向上 : 向上快速切换到未修改/未锁定行       
`Ctrl+down` Ctl+方向下 : 向下快速切换到未修改/未锁定行       
`Ctrl+left` Ctl+方向左 : 快速切换到水平行首       
`Ctrl+right` Ctl+方向右 : 快速切换到水平行尾 

#### 注意
`jieba`在打包时需要将`..Lib\site-packages\jieba\dict.txt`目录下的`jieba\dict.txt`拷贝到`fbs freeze`的打包目录下.

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request